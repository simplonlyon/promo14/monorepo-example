import {useEffect, useState} from 'react';


export function Home() {
    const [message,setMessage] = useState('');
    useEffect(() => {
        async function fetchMessage() {
            const response = await fetch('/api/example');
            const data = await response.json();
            setMessage(data.message);
        }
        fetchMessage();
    }, []);
    return (
        <p>Message from server : {message}</p>
    )
}