import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';
import './App.css';
import { About } from './pages/About';
import { Home } from './pages/Home';

function App() {
  return (
    <Router>

      <Link to="/">Home</Link>
      <Link to="/about">About</Link>

      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>
      </Switch>
    </Router>
    
  );
}

export default App;
