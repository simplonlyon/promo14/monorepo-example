import express from 'express';
import cors from 'cors';
import path from 'path';

export const server = express();
server.use(cors());

if(process.env.NODE_ENV === 'production') {

    server.use(express.static('../client/build'));

    server.use((req,res,next) => {
    
        if(req.path.startsWith('/api')) {
            next();
        } else {
            res.sendFile(path.resolve(__dirname+'/../../client/build/index.html'));
        }
    });
}


server.get('/api/example', (req, res) => res.json({message: 'coucou'}))