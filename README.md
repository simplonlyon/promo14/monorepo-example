# monorepo-example

Un monorepo node.js/express + react qui a comme particularité d'héberger le build react directement par le serveur express, ce qui a dû être géré par les points suivant :

* Dans le server express, définir que lorsque l'on est en production, on sert comme fichier static le dossier build du dossier client
* Pour faire que le routeur react fonctionne et ce même lorsque l'on recharge la page avec F5 par exemple, il a également fallu écrire un petit middleware qui redirige toutes les routes ne commençant par par `/api` vers le fichier index.html du dossier build toujours
* L'utilisation des workspaces npm a également demandé de modifier les version de jest et babel-jest côté server pour faire que celles ci soient compatibles avec celles requises par create-react-app
* Utilisation de la propriété proxy dans le package.json du client pour faire que les requêtes ajax pointent directement vers l'url du serveur pendant le développement, sans besoin d'avoir l'url complète

## Warnings
Cette configuration n'a pas été testée de manière extensive, il est donc tout à fait possible que des bugs surviennent dans certaines situations

## How to use
Lancer `npm start` pour démarrer les serveur de dev du client et du serveur en parallèle

Procfile et scripts npm prêts pour deployer sur heroku